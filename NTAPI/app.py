from networktables import NetworkTables
import time
from pip import Flask
from flask import jsonify
from flask_cors import CORS

app = Flask(__name__)
CORS(app)
STATE={}




@app.route("/")
def hello_world():
    return jsonify(STATE) 
    
NetworkTables.initialize(server='roborio-2096-frc.local')

def connectionListener(connected, info):
    print(info, "; Connection=%s" % connected)

def valueChanged(table, key, value, isNew):
    STATE[key]=value

NetworkTables.addConnectionListener(connectionListener, immediateNotify=True)


sd = NetworkTables.getTable('SmartDashboard')
sd.addEntryListener(valueChanged)

if __name__ == '__main__':
    app.run()